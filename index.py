from bs4 import BeautifulSoup as BS
from json import dump, load
from nltk.tokenize import WordPunctTokenizer as WPT
from platform import platform

cur_os = platform()
is_linux = 'Linux' in cur_os

try:
	from pymystem3 import Mystem as M
	print('Using pymystem.')
	m = M()	
except Exception as e:
	from pymorphy2 import MorphAnalyzer as MA
	ma = MA()
	print('Using pymorphy.')

wpt = WPT()
words = load(open('dict.json'))
tez = load(open('tez.json'))
stop_words = load(open('stop-words.json'))

def xml_to_dict(filename):
	db = {}
	for item in BS(open(filename).read(), 'html.parser').find_all('item'):
		try:
			assert item.code.text != ''
			db.update({
				item.code.text: {
					'title': item.title.text.lower(),
					'units': item.units.text,
					'price': item.price.text
				}
			})
		except Exception as e:
			print("[INFO] Empty xls row.")

	return db

def compare(S1,S2):
	ngrams = [S1[i:i+3] for i in range(len(S1))]
	# print(ngrams)
	count = 0
	for ngram in ngrams:
		count += S2.count(ngram)

	return count/max(len(S1), len(S2))

def unshorten(word0):
	max_c, max_word = 0,''
	for word1 in words:
		c = compare(word0, word1)
		c = c if c > 0.6 else 0
		if c > max_c :
			max_c = c
			max_word = word1
	return max_word

def tezaurus(word0):
	res = [word0]
	for word1, word2 in tez:
		c1, c2 = compare(word0, word1), compare(word0, word2)
		if c1 > 0.6 or c2 > 0.6:
			res += [word1, word2]
	return res

def title_to_index(title):
	rez = []
	try:
		for a in m.analyze(title):
			token = None
			try:
				token = a['analysis'][0]['lex']
			except IndexError:
				token = a['text'] # english word
			except KeyError:
				if a['text'] == '.':
					rez[-1] = unshorten(rez[-1])
			if token: rez += tezaurus(token)
	except NameError:
		for token in wpt.tokenize(title):
			max_score, max_token = 0, ''
			parsed = ma.parse(token) # temp line, rm when installing mystem
			for a in parsed:
				if 'PNCT' not in a.tag:
					if a.score > max_score:
						max_score = a.score
						max_token = a.normal_form
				elif token == '.':
					rez[-1] = unshorten(rez[-1])
			if max_token: rez += tezaurus(max_token)

	return tuple(set(rez))

def qs_to_index(qs):
	rez = []
	try:
		# print(m.analyze(qs))
		# TODO: cast to ONE type y and x in order to remove stop words from query
		y = set(m.lemmatize(qs))
		x = set(stop_words)
		r = ' '.join(y - x)
		for a in m.analyze(r):
			token = None
			try:
				token = a['analysis'][0]['lex']
			except IndexError:
				token = a['text'] # english word
			except KeyError:
				if a['text'] == '.':
					rez[-1] = unshorten(rez[-1])
			if token: rez += tezaurus(token)
			print(token)
	except NameError:
		y = set(wpt.tokenize(qs))
		x = set(stop_words)
		for token in y - x:
			max_score, max_token = 0, ''
			for a in ma.parse(token):
				if 'PNCT' not in a.tag:
					if a.score > max_score:
						max_score = a.score
						max_token = a.normal_form
				elif token == '.':
					rez[-1] = unshorten(rez[-1])
			if max_token: rez += tezaurus(max_token)
			print(max_token, token)
	return set(rez)

def tokenize_dict(db):
	index = {}
	for code in db:
		index.update({
			title_to_index(db[code]["title"]): code
		})

	return index

def search(dict_tokenized, qs):
	query_string = qs_to_index(qs)
	results = {}
	for dt in dict_tokenized:
		# print(i, x[y[i]])
		# print("%s: len(%s & set(%s)) > %s" % (len(qs & set(i)), qs, i, len(qs) / 2))
		# if len(qs & set(i)) > len(qs) / 2:
			# print(y[i], x[y[i]]['title'])
		w = 0
		for dt_elem in dt:
			a = 1
			for qs in query_string:
				c = compare(dt_elem, qs) + 1
				a *= c
			w += a
		val = w - len(dt)
		if val > (0.4 + len(query_string)/2):
			results.update({val: dt})
	# works, but different relevance values because of different count of words in dt!!
	return results

if __name__ == '__main__':
	x = xml_to_dict('database.xml')
	dict_tokenized = tokenize_dict(x)
	# search(dict_tokenized, 'фильтр воздушный')
	# search(dict_tokenized, 'фильтр маслянный')
	results = search(dict_tokenized, 'колодка тормозная передняя opel astra')
	# search(dict_tokenized, 'ремень генератора')
	# search(dict_tokenized, 'фильтр воздушный')

	for key, value in sorted(results.items(), reverse=True):
		print("%s: %s" % (key, value))


