from requests import get
from bs4 import BeautifulSoup as BS
from nltk.tokenize import WordPunctTokenizer as WPT
from rutermextract import TermExtractor as TE
from json import dump, dumps

DICT_URL = "http://autorelease.ru/articles/109-automobile/1578-slovar-avtomobilnuh-terminov.html"
WORD_URL = "http://autorelease.ru%s"

CONTENT_CLASS = "article-content clearfix"

wpt = WPT()
te = TE()

def ret_lst(soup):
	for li in soup.find(class_=CONTENT_CLASS)('li'):
		yield li

def get_definition(href):
	for w in te(BS(get(WORD_URL % href).content, 'html.parser').find(class_=CONTENT_CLASS).text.lower()):
		yield w.normalized

if __name__ == "__main__":
	dict_soup = BS(get(DICT_URL).content, 'html.parser')
	rez = {}
	for li in ret_lst(dict_soup):
		word = li.text
		href = li.a["href"]
		for word in get_definition(href):
			try:
				rez.update({word:rez[word] + 1})
			except KeyError:
				rez.update({word:1})
	print(dump(rez, open('dict.json', 'w'), ensure_ascii = 0, indent = 4))
